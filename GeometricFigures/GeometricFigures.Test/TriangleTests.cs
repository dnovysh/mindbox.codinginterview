﻿using GeometricFigures.Core;

using Xunit;

namespace GeometricFigures.Test
{
    public class TriangleTests
    {
        [Theory]
        [InlineData(3, 4, 5, 0)]
        [InlineData(3, 4.001, 5, 0.01)]
        public void IsRight_ShouldBeTrue(double a, double b, double c, double absoluteTolerance)
        {
            //arrange
            var triangle = new Triangle(a, b, c);
            //assert
            Assert.True(triangle.IsRight(absoluteTolerance));
        }

        [Theory]
        [InlineData(4, 5, 6, 0)]
        [InlineData(3, 4.001, 5, 0)]
        public void IsRight_ShouldBeFalse(double a, double b, double c, double absoluteTolerance)
        {
            //arrange
            var triangle = new Triangle(a, b, c);
            //assert
            Assert.False(triangle.IsRight(absoluteTolerance));
        }
    }
}
