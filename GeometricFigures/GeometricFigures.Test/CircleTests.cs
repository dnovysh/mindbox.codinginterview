using System;

using GeometricFigures.Core;

using Xunit;

namespace GeometricFigures.Test
{
    public class CircleTests
    {
        [Theory]
        [InlineData(-1)]
        [InlineData(-0.1)]
        public void Constructor_NegativeRadius_ThrowsArgumentException(double r)
        {
            //assert
            Assert.Throws<ArgumentException>(() => { var circle = new Circle(r); });
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(-0.1)]
        public void R_NegativeRadius_ThrowsArgumentException(double r)
        {
            //arrange
            var circle = new Circle(10);
            // act & assert
            Assert.Throws<ArgumentException>(() => circle.R = r);
        }

        [Theory]
        [InlineData(5.7, 102.07)]
        [InlineData(10.5, 346.36)]
        public void Area_Radius_ShouldBeEqualReferenceArea(double r, double expectedArea)
        {
            //arrange
            var circle = new Circle(r);
            //act
            var actualArea = Math.Round(circle.Area, 2, MidpointRounding.AwayFromZero);
            //assert
            Assert.Equal(expectedArea, actualArea);
        }
    }
}
