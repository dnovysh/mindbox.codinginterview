﻿using System;

namespace GeometricFigures.Core
{
    /// <summary>
    /// Невырожденный треугольник
    /// </summary>
    public class Triangle : IShape
    {
        private const string NegativeSideValueMessage =
            "Длина стороны должна быть больше или равна нулю";

        /// <summary>
        /// Проверяет длины сторон соответствию неравенства треугольника
        /// </summary>
        /// <param name="a">Длина стороны 1</param>
        /// <param name="b">Длина стороны 2</param>
        /// <param name="c">Длина стороны 3</param>
        /// <exception cref="ArgumentException">Генерируется, если значение
        /// длины хотя бы одной из сторон отрицательное или длины сторон 
        /// не удовлетворяют неравенству треугольника
        /// </exception>
        private static void CheckSides(double a, double b, double c)
        {
            if (a < 0) throw new ArgumentException(NegativeSideValueMessage, nameof(a));
            if (b < 0) throw new ArgumentException(NegativeSideValueMessage, nameof(b));
            if (c < 0) throw new ArgumentException(NegativeSideValueMessage, nameof(c));

            if (!(a + b > c && b + c > a && a + c > b)) 
            {
                throw new ArgumentException(
                    "Заданные стороны не удовлетворяют неравенству треугольника");
            }
        }

        private double _a;
        private double _b;
        private double _c;

        /// <summary>
        /// Длина стороны A
        /// </summary>
        /// <exception cref="ArgumentException">Генерируется если значение длины хотя бы одной из сторон
        /// не позволяет построить невырожденный треугольник</exception>
        public double A
        { 
            get => _a;
            set
            {
                CheckSides(value, B, C);
                _a = value;
            } 
        }

        /// <summary>
        /// Длина стороны B
        /// </summary>
        /// <exception cref="ArgumentException">Генерируется если значение длины хотя бы одной из сторон
        /// не позволяет построить невырожденный треугольник</exception>
        public double B
        {
            get => _b;
            set
            {
                CheckSides(A, value, C);
                _b = value;
            }
        }

        /// <summary>
        /// Длина стороны C
        /// </summary>
        /// <exception cref="ArgumentException">Генерируется если значение длины хотя бы одной из сторон
        /// не позволяет построить невырожденный треугольник</exception>
        public double C
        {
            get => _c;
            set
            {
                CheckSides(A, B, value);
                _c = value;
            }
        }

        /// <summary>
        /// Площадь треугольника
        /// </summary>
        /// <returns>Значение площади треугольника или double.NaN в случае,
        /// если полупериметр меньше одной из сторон треугольника</returns>
        public double Area
        {
            get
            {
                var p = (_a + _b + _c) / 2;
                return Math.Sqrt(p * (p - _a) * (p - _b) * (p - _c));
            }
        }

        /// <summary>
        /// Конструктор невырожденного треугольника
        /// </summary>
        /// <param name="a">Сторона A</param>
        /// <param name="b">Сторона B</param>
        /// <param name="c">Сторона C</param>
        /// <exception cref="Exception">Генерируется если значение длины хотя бы одной из сторон
        /// не позволяет построить невырожденный треугольник</exception>
        public Triangle(double a, double b, double c)
        {
            CheckSides(a, b, c);
            _a = a;
            _b = b;
            _c = c;
        }

        /// <summary>
        /// Проверяет, является ли треугольник прямоугольным
        /// </summary>
        /// <param name="absoluteTolerance">Максимальное абсолютное расхождение
        /// суммы квадратов катетов и квадрата гипотенузы, по умолчанию 0</param>
        /// <returns>true в случае если треугольник прямоугольный, false в противном случае</returns>
        /// <exception cref="ArgumentException">Генерируется, если заданное максимальное расхождение
        /// суммы квадратов катетов и квадрата гипотенузы отрицательное</exception>
        public bool IsRight(double absoluteTolerance = 0)
        {
            if (absoluteTolerance < 0)
            {
                throw new ArgumentException(
                    "Отрицательное значение абсолютной точности", nameof(absoluteTolerance));
            }

            var aa = _a * _a;
            var bb = _b * _b;
            var cc = _c * _c;

            return (
                       (Math.Abs(aa + bb - cc) <= absoluteTolerance) ||
                       (Math.Abs(aa + cc - bb) <= absoluteTolerance) ||
                       (Math.Abs(cc + bb - aa) <= absoluteTolerance)
                   );
        }
    }
}
