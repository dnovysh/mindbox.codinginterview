﻿using System;

namespace GeometricFigures.Core
{
    /// <summary>
    /// Круг
    /// </summary>
    public class Circle : IShape
    {
        private double _r;

        /// <summary>
        /// Радиус круга
        /// </summary>
        /// <exception cref="ArgumentException">Генерируется при отрицательном значении радиуса</exception>
        public double R
        { 
            get => _r;
            set 
            {
                if (value < 0)
                {
                    throw new ArgumentException(
                        "Значение радиуса круга должно быть большим или равным нулю");
                }

                _r = value;
            }
        }

        /// <summary>
        /// Площадь круга
        /// </summary>
        public double Area { get => Math.PI * _r * _r; }

        /// <summary>
        /// Конструктор класса Круг
        /// </summary>
        /// <param name="r"></param>
        /// <exception cref="ArgumentException">Генерируется при отрицательном значении радиуса</exception>
        public Circle(double r)
        {
            R = r;
        }
    }
}
