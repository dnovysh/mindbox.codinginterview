﻿namespace GeometricFigures.Core
{
    /// <summary>
    /// Геометрическая фигура
    /// </summary>
    public interface IShape
    {
        /// <summary>
        /// Площадь геометрической фигуры
        /// </summary>
        double Area { get; }
    }
}
